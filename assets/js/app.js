const txtNombre = document.querySelector('.txtNombre');
const btnBuscar = document.querySelector('.btnBuscar');
const warning = document.querySelector('.warning');
const lblNombre = document.querySelector('.lblNombre');
const lblAno = document.querySelector('.lblAno');
const imagenPelicula = document.querySelector('.imagen__pelicula');
const resena = document.querySelector('.resena');
const actores = document.querySelector('.actores');

const cargarPelicula = (pelicula) => {
    if(pelicula.Title != txtNombre.value) {
        warning.textContent = "Ocurrio un error. Asegurate de que el nombre sea correcto";
        return;
    }
    warning.textContent = "";
    lblNombre.textContent = pelicula.Title;
    lblAno.textContent = pelicula.Year;
    imagenPelicula.src = pelicula.Poster;
    resena.textContent = pelicula.Plot;
    actores.textContent = pelicula.Actors;
}

const fetchPelicula = (pelicula) => {
    fetch(`http://www.omdbapi.com/?t=${pelicula}&plot=full&apikey=30063268`)
    .then(res => res.json())
    .then(data => cargarPelicula(data))
    .catch(err => console.log('Surgió un error'));
}

const buscarPelicula = (e) => {
    e.preventDefault();
    fetchPelicula(txtNombre.value);
}

btnBuscar.addEventListener('click', buscarPelicula);